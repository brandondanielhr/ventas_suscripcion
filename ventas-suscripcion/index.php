<?php

require_once "controladores/plantilla.controlador.php";
require_once "controladores/ruta.controlador.php";

require_once "backoffice/controladores/usuario_controlador.php";
require_once "backoffice/modelos/usuarios_modelo.php";


// https://github.com/PHPMailer/PHPMailer
require_once "backoffice/extensiones/vendor/autoload.php";

$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();