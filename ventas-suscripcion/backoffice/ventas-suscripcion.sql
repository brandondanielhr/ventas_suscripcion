-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-03-2020 a las 23:48:08
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ventas-suscripcion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `perfil` text NOT NULL,
  `nombre` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `suscripcion` int(11) NOT NULL,
  `id_suscripcion` text,
  `ciclo_pago` int(11) DEFAULT NULL,
  `vencimiento` date DEFAULT NULL,
  `verificacion` int(11) NOT NULL,
  `email_encriptado` text,
  `foto` text,
  `enlace_afiliado` text,
  `patrocinador` text,
  `paypal` text NOT NULL,
  `pais` int(11) DEFAULT NULL,
  `cod_pais` text,
  `telefono_movil` text,
  `firma` text,
  `fecha_contrato` date DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `perfil`, `nombre`, `email`, `password`, `suscripcion`, `id_suscripcion`, `ciclo_pago`, `vencimiento`, `verificacion`, `email_encriptado`, `foto`, `enlace_afiliado`, `patrocinador`, `paypal`, `pais`, `cod_pais`, `telefono_movil`, `firma`, `fecha_contrato`, `fecha`) VALUES
(1, 'admin', 'academy of line', 'brandondanielhh@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auXBm1Vr2M1NV5t/zNQtGHGpS5fFirrbG', 1, NULL, NULL, NULL, 1, NULL, 'vistas/img/usuarios/1/840.jpg', 'academy-of-life', NULL, 'brandondanielhr@gmail.com', NULL, NULL, NULL, NULL, NULL, '2020-03-17 15:05:42'),
(2, 'usuarios', 'Daniel', 'brandondanielhr@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auvZW0rfHE2Ya6aIoJotaXL8n4qu77GIa', 0, NULL, NULL, NULL, 1, 'acec939d6a603743e9443fc74d683ef4', 'vistas/img/usuarios/2/867.png', NULL, 'academy-of-life', 'Eduardogevara@gmail.com', NULL, NULL, NULL, NULL, NULL, '2020-03-24 16:47:06'),
(3, 'usuarios', 'brandon', 'Leonardo@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aucfKA7IzVT/3iOQo4lfhWsxD1bbYDD.6', 0, NULL, NULL, NULL, 1, '48a44670275f0cd3a80f53f42630f66f', 'vistas/img/usuarios/3/683.png', NULL, 'academy-of-life', '', NULL, NULL, NULL, NULL, NULL, '2020-03-24 19:08:41'),
(4, 'usuarios', 'jorge gaitan', 'Jorgegaitan@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auruyC1rKJu6lE8ZPTHTKfAzmnBcWK5tC', 0, NULL, NULL, NULL, 1, 'bcd949b48620e934277f6e1b77a1abba', 'vistas/img/usuarios/4/994.jpg', NULL, 'academy-of-life', '', NULL, NULL, NULL, NULL, NULL, '2020-03-30 17:17:47');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
