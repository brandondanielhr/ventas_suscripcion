<?php
require_once "../controladores/controlador_general.php";
require_once "../controladores/usuario_controlador.php";
require_once "../modelos/usuarios_modelo.php";


class ajaxUsuarios{
/*=============================================
Validar Email Repetido
=============================================*/
	public $validarEmail;
	public function ajaxValidaEmail(){
		$item='email';
		$valor=$this->validarEmail;
		
		$respuesta=	ControladorUsuarios::ctrMostrarUsuarios($item,$valor);

		echo json_encode($respuesta);

	} 

/*=============================================
Suscripcion Paypal
=============================================*/
	public $Suscripcion;
	public $email;
	public $nombre;

	public function ajaxSuscripcion(){
		$ruta=ControladorGeneral::ctrRuta();
		$ValorSuscripcion=ControladorGeneral::ctrValorSuscripcion();
		//formato para fecha y hora completa actual
		$fecha=substr(date("c"),0,-6)."Z";
		
		/*=============================================
		Crear el acces_token
		=============================================*/

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.sandbox.paypal.com/v1/oauth2/token",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 300,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "grant_type=client_credentials",
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic QVM0ZWRxZktwZ25hTjJEbk50X3Y1bU5NVFVfMVpFaEZ0S2Q2YTRfNjVVRkttdHlZUHl0TXNzekRlQXdnYXQ2NGRSbWpLSmFVSGRXcjZYQm86RUJjbFZPam91RGl4Wi1qMjlUWWtrQWRGOWVFN1JzWUtGYkdXMExjUnJrUHVUNGM3WS14UVhtOEdLaVFUeTN6aVlDNXdGYXdFZUYtWk9zQlY=",
				"Content-Type: application/x-www-form-urlencoded"
				),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			//el parametro true sirve para capturar propiedades
			$respuesta=json_decode($response,true);
			$token=$respuesta['access_token'];
			
			
			/*=============================================
			Crear el producto de la api de paypal
			=============================================*/
			$curl2 = curl_init();

			curl_setopt_array($curl2, array(
				CURLOPT_URL => "https://api.sandbox.paypal.com/v1/catalogs/products",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 300,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "{\r\n  \"name\": \"Academy of life\",\r\n  \"description\": \"Educación en línea\",\r\n  \"type\": \"DIGITAL\",\r\n  \"category\": \"EDUCATIONAL_AND_TEXTBOOKS\",\r\n  \"image_url\": \"".$ruta."backoffice/vistas/img/plantilla/icono.png\",\r\n  \"home_url\": \"".$ruta."\"\r\n}",
				CURLOPT_HTTPHEADER => array(
					"authorization: Bearer ".$token,
					"cache-control: no-cache",
					"content-type: application/json"
					),
				));


			/*=============================================
			Crear el plan de pago
			=============================================*/

			$response = curl_exec($curl2);

			curl_close($curl2);
			$respuesta2=json_decode($response,true);
			$id_producto=$respuesta2['id'];
			
			$curl3 = curl_init();

			curl_setopt_array($curl3, array(

			  CURLOPT_URL => "https://api.sandbox.paypal.com/v1/billing/plans",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 300,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>"{\r\n  \"product_id\": \"".$id_producto."\",\r\n  \"name\": \"Inscripcion Mensual a acadmy of life\",\r\n  \"description\": \"Plan de pago mensual\",\r\n  \"status\": \"ACTIVE\",\r\n  \"billing_cycles\": [\r\n    {\r\n      \"frequency\": {\r\n        \"interval_unit\": \"MONTH\",\r\n        \"interval_count\": 1\r\n      },\r\n      \"tenure_type\": \"TRIAL\",\r\n      \"sequence\": 1,\r\n      \"total_cycles\": 1,\r\n      \"pricing_scheme\": {\r\n        \"fixed_price\": {\r\n          \"value\": \"".$ValorSuscripcion."\",\r\n          \"currency_code\": \"USD\"\r\n        }\r\n      }\r\n    },\r\n    {\r\n      \"frequency\": {\r\n        \"interval_unit\": \"MONTH\",\r\n        \"interval_count\": 1\r\n      },\r\n      \"tenure_type\": \"REGULAR\",\r\n      \"sequence\": 2,\r\n      \"total_cycles\": 99,\r\n      \"pricing_scheme\": {\r\n        \"fixed_price\": {\r\n          \"value\": \"10\",\r\n          \"currency_code\": \"USD\"\r\n        }\r\n      }\r\n    }\r\n  ],\r\n  \"payment_preferences\": {\r\n    \"auto_bill_outstanding\": true,\r\n    \"setup_fee_failure_action\": \"CONTINUE\",\r\n    \"payment_failure_threshold\": 3\r\n  },\r\n  \"taxes\": {\r\n    \"percentage\": \"10\",\r\n    \"inclusive\": false\r\n  }\r\n}",
			  CURLOPT_HTTPHEADER => array(
			  "authorization: Bearer ".$token,
				"cache-control: no-cache",
				"content-type: application/json"
			  ),
			));

			$response = curl_exec($curl3);

			curl_close($curl3);
			$respuesta3=json_decode($response,true);
			$id_plan=$respuesta3['id'];

			$curl4 = curl_init();

			curl_setopt_array($curl4, array(
			  CURLOPT_URL => "https://api.sandbox.paypal.com/v1/billing/subscriptions",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 300,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>"{\r\n  \"plan_id\": \"".$id_plan."\",\r\n  \"start_time\": \"".$fecha."\",\r\n  \"subscriber\": {\r\n    \"name\": {\r\n      \"given_name\": \"".$this->nombre."\"\r\n    },\r\n    \"email_address\": \"".$this->email."\"\r\n  },\r\n  \"auto_renewal\": true,\r\n  \"application_context\": {\r\n    \"brand_name\": \"Academy of life\",\r\n    \"locale\": \"en-US\",\r\n    \"shipping_preference\": \"SET_PROVIDED_ADDRESS\",\r\n    \"user_action\": \"SUBSCRIBE_NOW\",\r\n    \"payment_method\": {\r\n      \"payer_selected\": \"PAYPAL\",\r\n      \"payee_preferred\": \"IMMEDIATE_PAYMENT_REQUIRED\"\r\n    },\r\n    \"return_url\": \"".$ruta."backoffice/index.php?pagina=perfil\",\r\n    \"cancel_url\": \"".$ruta."backoffice/index.php?pagina=perfil\"\r\n  }\r\n}",
			  CURLOPT_HTTPHEADER => array(
			   "authorization: Bearer ".$token,
				"cache-control: no-cache",
				"content-type: application/json"
			  ),
			));

	
			$response = curl_exec($curl4);
			$err = curl_error($curl4);

			curl_close($curl4);

			if($err) {
				echo "cURL Error #:" . $err;

			}else{

				$respuesta4 = json_decode($response, true);

				$urlPaypal = $respuesta4["links"][0]["href"];

				echo $urlPaypal;
			}

			

				
	}

	/*=============================================
	Cancelar Suscripcion
	=============================================*/
	public $id_usuario;
	
	public function cancelarSuscripcion(){
		

		$valor= $this->id_usuario;
		
		$respuesta = ControladorUsuarios::ctrCancelarSuscripcion($valor);

		echo $respuesta;




	}

}

if(isset($_POST['ValidarEmail'])){
	$valEmail= new ajaxUsuarios();
	//a mi objeto le asigno la propiedad/attr=validarEmail y le doy el valor de $_POST['ValidarEmail']
	$valEmail-> validarEmail = $_POST['ValidarEmail'];
	$valEmail-> ajaxValidaEmail();

}
/*=============================================
Suscripcion Paypal
=============================================*/
if(isset($_POST['suscripcion']) && $_POST['suscripcion'] == "ok"){

	$paypal= new ajaxUsuarios();
	$paypal-> nombre  = $_POST['nombre'];
	$paypal-> email  = $_POST['email'];
	$paypal->ajaxSuscripcion();

}
/*=============================================
Cancelar Suscripcion
=============================================*/

if(isset($_POST['id_usuario'])){
	$cancelarUsuario= new ajaxUsuarios();
	$cancelarUsuario-> id_usuario = $_POST['id_usuario'];
	$cancelarUsuario->cancelarSuscripcion();	
}


?>

