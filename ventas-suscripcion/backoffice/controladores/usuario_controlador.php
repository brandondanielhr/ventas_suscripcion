<?php
// https://github.com/PHPMailer/PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class ControladorUsuarios{

	public function ctrRegistroUsuario(){

		if(isset($_POST["registro_nombre"])){
			$ruta=ControladorRuta::ctrRuta();

			//preg_match metemos el valor a evaluar y lo comparamos con una expresion r.
			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["registro_nombre"]) &&
				preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["registro_email"]) &&
			    preg_match('/^[a-zA-Z0-9]+$/', $_POST["registro_password"])){


				//hash blowfish $2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
				$encriptar= crypt($_POST['registro_password'],
					'$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');


				$encriptar_Email=md5($_POST["registro_email"]);


				$tabla="usuarios";
				$datos=array("perfil"=>"usuarios",
							"nombre"=>$_POST["registro_nombre"],
							"email"=>$_POST["registro_email"],
							"password"=>$encriptar,
							"suscripcion"=>0,
							"verificacion"=>0,
							"encriptar_Email"=>$encriptar_Email,
							"patrocinador"=>$_POST["patrocinador"]);

				$respuesta=ModeloUsuario::mdlRegistroUsuario($tabla,$datos);

				

				if($respuesta == "ok" ){


					/*=============================================
					verificacion correo
					=============================================*/
					//fecha de nuestro servidor timezones

					date_default_timezone_set("America/Argentina/Buenos_Aires");
					

					$email=new PHPMailer;

					// tipo de caracteres
					$email->Charset="UTF-8";

					//funcion para establecer como enviar el correo. por smpt,mail etc
					$email->isMail();
					//de que correo se enviar la info
					$email->setFrom("brandondanielhr@gmail.com","La_Suiza");	
					//si se necesita enviar una respuesta...es bueno dejar la misma dirección que el From, para no caer en spam
					$email->addReplyTo("brandondanielhr@gmail.com","La_Suiza");
					//Asunto
					$email->Subject= "¡Porfavor verifique el correo electronico!";
					//email al que se le envia
					$email->addAddress($_POST["registro_email"]);
					//plantilla de email
					$email->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
	
										<center>
												
											<img style="padding:20px; width:10%" src="https://tutorialesatualcance.com/tienda/logo.png">

										</center>

										<div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
											
											<center>

												<img style="padding:20px; width:15%" src="https://tutorialesatualcance.com/tienda/icon-email.png">

												<h3 style="font-weight:100; color:#999">VERIFIQUE SU DIRECCIÓN DE CORREO ELECTRÓNICO</h3>

												<hr style="border:1px solid #ccc; width:80%">

												<h4 style="font-weight:100; color:#999; padding:0 20px">Para comenzar a usar su cuenta, debe confirmar su dirección de correo electrónico</h4>

												<a href="'.$ruta.$encriptar_Email.'" target="_blank" style="text-decoration:none">
													
													<div style="line-height:60px; background:#0aa; width:60%; color:white">Verifique su dirección de correo electrónico</div>

												</a>

												<br>

												<hr style="border:1px solid #ccc; width:80%">

												<h5 style="font-weight:100; color:#999">Si no se inscribió en esta cuenta, puede ignorar este correo electrónico y eliminarlo.</h5>



											</center>	



										</div>


									</div>');


					$enviar=$email->send();

						if(!$enviar){
							echo '<script>
							swal({
								type:"error",
								title:"ERROR",
								text:"¡Ha ocurrido un problema enviando verificacion de correo electronico a '.$_POST['registro_email'].' '.$email->ErrorInfo.',por favor intentelo de nuevo",
								showComfirmButton:"True",
								ComfirmButtonText:"Cerrar"

							}).then(function(result){

								if(result.value){
									history.back();
									
								}

								})

							</script>';
						}


						else{
							echo '<script>
								swal({
									type:"success",
									title:"¡SU CUENTA SE CREO EXITOSAMENTE!",
									text:"¡Porfavor revise  la bandeja de entrada o en su defecto la carpeta de SPAM de su correo electronico para verificar la cuenta!",
									showComfirmButton:"True",
									ComfirmButtonText:"Cerrar"

								}).then(function(result){

									if(result.value){
										window.location ="'.$ruta.'ingreso";
										
									}

									})

								</script>';
						}
				}

			}else{

				echo'<script>
					swal({
						type:"error",
						title:"¡CORREGIR!",
						text:"¡No se permiten campos especiales!",
						showComfirmButton:"True",
						ComfirmButtonText:"Cerrar"

					}).then(function(result){

						if(result.value){
							history.back();
						}

					});

					</script>';

					
				}

		}

		
	}

/*=============================================
Validar Email Repetido
=============================================*/

	static public function ctrMostrarUsuarios($item,$valor){


		$tabla="usuarios";

		$respuesta = ModeloUsuario::mdlMostrarUsuarios($tabla,$item,$valor);
		
		return $respuesta;

	}

/*=============================================
Actualizar usuario
=============================================*/

	static public function ctrActualizarUsuario($id,$item,$valor){

		$tabla="usuarios";

		$respuesta =ModeloUsuario::mdlActualizarUsuario($id,$item,$valor,$tabla);

		return $respuesta;

	}


/*=============================================
Ingreso usuario
=============================================*/
	
	static public function ctrIngresoUsuario(){
		if(isset($_POST['ingreso_email'])){

			if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["ingreso_email"]) &&
			preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingreso_password"])){

				$encriptar= crypt($_POST['ingreso_password'],
					'$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$tabla='usuarios';	
				$item='email';
				$valor=$_POST["ingreso_email"];

				$respuesta=ModeloUsuario::mdlMostrarUsuarios($tabla,$item,$valor);

				if($_POST["ingreso_email"] == $respuesta["email"] && 
					$respuesta["password"] == $encriptar){

					if($respuesta['verificacion'] == 0){

						echo'<script>
								swal({
									type:"error",
									title:"¡CORREGIR!",
									text:"¡El correo electronico aun no ha sido verificado!",
									showComfirmButton:"True",
									ComfirmButtonText:"Cerrar"

								}).then(function(result){

									if(result.value){
										history.back();
									}

								});

							</script>';	
						return;	
					}else{

						$_SESSION['validar'] = "ok";
						$_SESSION['id']=$respuesta['id_usuario'];

						$ruta=ControladorRuta::ctrRuta(); 	

						echo'<script>

							window.location ="'.$ruta.'backoffice";



						</script>';

					}

				}else{

				echo'<script>
					swal({
						type:"error",
						title:"¡CORREGIR!",
						text:"¡Hubo un error,verifique que su email y contraseña sean correctos!",
						showComfirmButton:"True",
						ComfirmButtonText:"Cerrar"

					}).then(function(result){

						if(result.value){
							history.back();
						}

					});

					</script>';

				}


			}
		
		}
	
	}



/*=============================================
Cambiar Imagen
=============================================*/
	static public function ctrCambiarImagen(){

		if(isset($_POST['id_usuario_foto'])){

			$ruta=$_POST['foto_actual'];

			if(isset($_FILES['cambiar_foto']['tmp_name']) && 
			!empty($_FILES['cambiar_foto']['tmp_name'])){
				list($ancho,$alto) = getimagesize($_FILES['cambiar_foto']['tmp_name']);

				$new_Ancho=500;
				$new_alto=500;

				/*=========================================================
				Creamos el directorio donde guardaremos la foto del usuario
				==========================================================*/				

				$directorio="vistas/img/usuarios/".$_POST['id_usuario_foto'];
				
				
				/*==================================================================
				Preguntamos si hay coincidencia con esa imagen en la BD y la carpeta
				==================================================================*/		
				//vienen archivos ??
				if($ruta != ""){
					//borraria la foto existente
					unlink($ruta);
				}else{
					//no existe el directorio
					if(!file_exists($directorio)){
						//crear una carpeta
						mkdir($directorio,0755);	
					
					}
			
				}

				/*==================================================================
				De acuerdo con el tipo de img Aplicamos las funciones por defecto de php
				==================================================================*/

				if($_FILES['cambiar_foto']['type'] == 'image/jpeg'){

					$aleatorio=mt_rand(50,999);

					$ruta=$directorio."/".$aleatorio.".jpg";

					//Creo una nueva imagen a traves de un fichero o 	de una url
					$origen= imagecreatefromjpeg($_FILES['cambiar_foto']['tmp_name']);

					$destino= imagecreatetruecolor($new_Ancho, $new_alto);
					
					imagecopyresized($destino,$origen,0,0,0,0,$new_Ancho,$new_alto,$ancho,$alto);

					imagejpeg($destino,$ruta);
				
				}else if($_FILES['cambiar_foto']['type'] == 'image/png'){

					$aleatorio=mt_rand(50,999);
					
					$ruta=$directorio."/".$aleatorio.".png";
					
					//Crea una nueva imagen a partir de un fichero o de una URL
					$origen= imagecreatefrompng($_FILES['cambiar_foto']['tmp_name']);
					
					$destino= imagecreatetruecolor($new_Ancho, $new_alto);
					//Establece el modo de mezcla para una imagen
					imagealphablending($destino,FALSE);
					
					imagesavealpha($destino,TRUE);


					//Copia y cambia el tamaño de parte de una imagen
					imagecopyresized($destino,$origen,0,0,0,0,$new_Ancho,$new_alto,$ancho,$alto);


					/*Exportar la imagen al navegador o a un fichero
					Parámetros 
					image
					Un recurso image, es devuelto por una de las funciones de creación de imágenes, como imagecreatetruecolor().

					to
					La ruta o un recurso de flujo de apertura (el cual se cierra automáticamente después de que devuelva esta función) donde guardar el fichero. Si no se establece, o su valor es NULL, se mostrará directamente en la salida el flujo de imagen sin tratar.*/


					imagepng($destino,$ruta);


				}else{
					
					echo'<script>
					swal({
						type:"error",
						title:"¡CORREGIR!",
						text:"¡No se permiten formatos distintos a JPG/PNG!",
						showComfirmButton:"True",
						ComfirmButtonText:"Cerrar"

					}).then(function(result){

						if(result.value){
							history.back();
						}

					});

					</script>';
					return;

					

				}

			}

			//final condicion
			$tabla="usuarios";
			$id=$_POST['id_usuario_foto'];
			$item="foto";
			$valor=$ruta;

			$respuesta = ModeloUsuario::mdlActualizarUsuario($id,$item,$valor,$tabla);

			if($respuesta=="ok"){
				echo '<script>
						swal({
							type:"success",
							title:"¡Correcto!",
							text:"¡La foto ha sido Actualizada!",
							showComfirmButton:"True",
							ComfirmButtonText:"Cerrar"

						}).then(function(result){

							if(result.value){
								history.back()
							}

						})

				</script>';
			}

		}

	}


	static public function ctrCambiarPassword(){

		if(isset($_POST['id_usuario_password'])){
			if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["editar_password"])){

				$encriptar= crypt($_POST['editar_password'],
					'$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$tabla="usuarios";
				$id=$_POST['id_usuario_password'];
				$item="password";
				$valor=$encriptar;

				$respuesta = ModeloUsuario::mdlActualizarUsuario($id,$item,$valor,$tabla);

				if($respuesta=="ok"){
					echo '<script>
							swal({
								type:"success",
								title:"¡Correcto!",
								text:"¡La Contraseña ha sido Actualizada!",
								showComfirmButton:"True",
								ComfirmButtonText:"Cerrar"

							}).then(function(result){

								if(result.value){
									history.back()
								}

							})

					</script>';
			
				}
			}else{

				echo'<script>
					swal({
						type:"error",
						title:"¡CORREGIR!",
						text:"¡No se permiten campos especiales!",
						showComfirmButton:"True",
						ComfirmButtonText:"Cerrar"

					}).then(function(result){

						if(result.value){
							history.back();
						}

					});

				</script>';


			}
		}

	}


	/*==================================================================
	Recuperar Contraseña
	==================================================================*/	

	public static function ctrRecuperarPassword(){

		if(isset($_POST['emailRecuperarContraseña'])){
			
			if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["emailRecuperarContraseña"])){
				$ruta=ControladorRuta::ctrRuta();
				/*==================================================================
				Generar Contraseña aleatoria
				==================================================================*/
				function generarPassword($longitud){
					$password="";

					$patron="1234567890abcdefghijklmnopqrstuvwxyz";
					$max=strlen($patron)-1;

					for($i=0;$i<$longitud;$i++){

						$password .= $patron{mt_rand(0,$max)};
						echo $password;	

					}	
					
					return $password;

				}

				$newpassword=generarPassword(11);
				
				$encriptar=crypt($newpassword,'$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
				
				$tabla="usuarios";
				
				$item="email";	
				
				$valor=$_POST['emailRecuperarContraseña'];

				$TraerUsuario= ModeloUsuario::mdlMostrarUsuarios($tabla,$item,$valor);
				if($TraerUsuario){
					$tabla="usuarios";
					$valor=$encriptar;
					$item="password";	
					$id=$TraerUsuario['id_usuario'];

					$respuesta= ModeloUsuario::mdlActualizarUsuario($id,$item,$valor,$tabla);
					
					if($respuesta="ok"){
						/*=============================================
						verificacion correo
						=============================================*/
						//fecha de nuestro servidor timezones

						date_default_timezone_set("America/Argentina/Buenos_Aires");
								

						$mail=new PHPMailer;

						// tipo de caracteres
						$mail->Charset="UTF-8";

						//funcion para establecer como enviar el correo. por smpt,mail etc
						$mail->isMail();
						//de que correo se enviar la info
						$mail->setFrom("brandondanielhr@gmail.com","La_Suiza");	
						//si se necesita enviar una respuesta...es bueno dejar la misma dirección que el From, para no caer en spam
						$mail->addReplyTo("brandondanielhr@gmail.com","La_Suiza");
						//Asunto
						$mail->Subject= "¡Nueva Contraseña!";
						//email al que se le envia
						$mail->addAddress($TraerUsuario['email']);
						//plantilla de email
						$mail->msgHTML('<div style="width:100%; 
											background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
	
											<center>
												
												<img style="padding:20px; width:10%" src="https://tutorialesatualcance.com/tienda/logo.png">

											</center>

											<div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
											
												<center>
												
												<img style="padding:20px; width:15%" src="https://tutorialesatualcance.com/tienda/icon-pass.png">

												<h3 style="font-weight:100; color:#999">SOLICITUD DE NUEVA CONTRASEÑA</h3>

												<hr style="border:1px solid #ccc; width:80%">

												<h4 style="font-weight:100; color:#999; padding:0 20px"><strong>Su nueva contraseña: </strong>'.$newpassword.'</h4>

												<a href="'.$ruta.'ingreso" target="_blank" style="text-decoration:none">

												<div style="line-height:30px; background:#0aa; width:60%; padding:20px; color:white">			
													Haz click aquí
												</div>

												</a>

												<h4 style="font-weight:100; color:#999; padding:0 20px">Ingrese nuevamente al sitio con esta contraseña y recuerde cambiarla en el panel de perfil de usuario</h4>

												<br>

												<hr style="border:1px solid #ccc; width:80%">

												<h5 style="font-weight:100; color:#999">Si no se inscribió en esta cuenta, puede ignorar este correo electrónico y la cuenta se eliminará.</h5>

												</center>

											</div>

										</div>');


						$enviar=$mail->send();

						if(!$enviar){
							echo '<script>
								swal({
									type:"error",
									title:"ERROR",
									text:"¡Ha ocurrido un problema enviando verificacion de correo electronico a '.$TraerUsuario['email'].' '.$email->ErrorInfo.',por favor intentelo de nuevo",
									showComfirmButton:"True",
									ComfirmButtonText:"Cerrar"

								}).then(function(result){

									if(result.value){
										history.back();					
									}
								})

							</script>';
						


						}else{
							
							echo'<script>
								swal({
									type:"success",
									title: "¡SU NUEVA CONTRASEÑA HA SIDO ENVIADA!",
									text: "¡Por favor revise la bandeja de entrada o la carpeta SPAM de su correo electrónico para tomar la nueva contraseña!",
									showConfirmButton: true,
									confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){

										window.location = "'.$ruta.'ingreso";

									}


								});	

							</script>';
						}

					}
					

				}else{
						
					echo '<script>
						swal({
							type:"error",
							title: "¡ERROR!",
							text: "¡El correo no existe en el sistema, puede registrase nuevamente con ese correo!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"
							  
						}).then(function(result){
							if(result.value){   
								history.back();
							} 
						});

					</script>';

				}

			}else{

				echo'<script>
					swal({
						type:"error",
						title:"¡CORREGIR!",
						text:"¡Error al escribir en el correo!",
						showComfirmButton:"True",
						ComfirmButtonText:"Cerrar"

					}).then(function(result){

						if(result.value){
							history.back();
						}

					});

									
				</script>';
			}		

		}		
	}
	/*==================================================================
	Iniciar Suscripcion 
	==================================================================*/	

	static public function ctrIniciarSuscripcion($datos){

		$tabla='usuarios';
		$respuesta=ModeloUsuario::mdlIniciarSuscripcion($tabla,$datos);


		return $respuesta;

	}

	static public function ctrCancelarSuscripcion($valor){
		$tabla='usuarios';
		$datos=array("id_usuario"=> $valor,
					"suscripcion"=> 0,
					"ciclo_pago"=> null,
					"firma"=> null,
					"fecha_contrato"=> null);
		
		$respuesta=ModeloUsuario::mdlCancelarSuscripcion($tabla,$datos);
		
		return $respuesta;

	}



}



?>