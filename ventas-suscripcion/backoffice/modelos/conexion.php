<?php
class conexion{

	//private $conexion;

 	static public function conectar(){

 		try{
 			$conexion= new PDO("mysql:host=localhost;dbname=ventas-suscripcion","root","");
 			//ERRMODE_EXCEPTION,establecerá sus propiedades para luego poder reflejar el error y su información(esconde datos que podrían ayudar a alguien a atacar tu aplicación)
 			$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 			
 		}	
		
		catch(PDOException $e){
		echo $e->getMessage();
		return;
		}

		//que se ejecute la conexion( y me respete el acotejamiento) 
		$conexion->exec("set names utf8");	
		return $conexion;


		$conexion=null;
	//metodo destructor
	/*function __destroy(){
		$conexion=null;
		mysql_close( $this -> $conexion );
	}*/


	}
	/*static function closeConnection(&$conexion) {
        $conexion=null;
    }*/

}

?>