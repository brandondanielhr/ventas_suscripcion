<?php
require_once "conexion.php";

class ModelosAcademia{
	/*=============================================
	MOSTRAR CATEGORIAS 
	=============================================*/

	static public function mdlMostrarCategorias($item,$valor,$tabla){

		if($item != null && $valor != null){

			$smt= Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE 
			$item=:$item");

			$smt->bindParam(":".$item,$valor,PDO::PARAM_STR);

			$smt->execute();

			return $smt-> FetchAll();
		}else{

			$smt= Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$smt->execute();

			return $smt-> FetchAll();
		}

		$smt->close();

		$smt = null;



	}

	static public function mdlMostrarAcademia($tabla1,$tabla2,$item,$valor){

		if($item != null && $valor != null){
			$smt = Conexion::conectar()->prepare("SELECT $tabla1.*,$tabla2.* FROM 
				$tabla1 inner join $tabla2 on $tabla1.id_categoria = $tabla2.id_cat WHERE $item=:$item");
			
			$smt->bindParam(":".$item,$valor,PDO::PARAM_STR);

			$smt->execute();	

			return $smt->FetchAll();

		}else{
			$smt= Conexion::conectar()->prepare("SELECT $tabla1.*,$tabla2.* FROM $tabla1 inner join 
				$tabla2 on $tabla1.id_categoria = $tabla2.id_cat");

			$smt->execute();	

			return $smt->FetchAll();

		}
		$smt->close();//cerramos la conexion

		$smt = null; //vaciamos la conexion

	}
	static public function mdlMostrarvideo($item,$valor,$tabla){

		if($item != null && $valor != null){

			$smt= Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE 
			$item=:$item");

			$smt->bindParam(":".$item,$valor,PDO::PARAM_STR);

			$smt->execute();

			return $smt-> Fetch();
		}else{

			$smt= Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$smt->execute();

			return $smt-> FetchAll();
		}

		$smt->close();

		$smt = null;
	}	




}

?>