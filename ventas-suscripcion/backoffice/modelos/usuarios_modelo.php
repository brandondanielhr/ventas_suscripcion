<?php

require_once "conexion.php";

class ModeloUsuario{

	static public function mdlRegistroUsuario($tabla,$datos){

		$stmt= conexion::conectar()->prepare("INSERT INTO $tabla
											(perfil, nombre, email, password, 
											suscripcion, verificacion, email_encriptado, patrocinador) 
											
											VALUES(:perfil, :nombre, :email, 
											:password, :suscripcion, :verificacion,
											:encriptar_Email, :patrocinador)");

		$stmt->bindParam(":perfil",$datos['perfil'],PDO::PARAM_STR);
		$stmt->bindParam(":nombre",$datos['nombre'],PDO::PARAM_STR);
		$stmt->bindParam(":email",$datos['email'],PDO::PARAM_STR);
		$stmt->bindParam(":password",$datos['password'],PDO::PARAM_STR);
		$stmt->bindParam(":suscripcion",$datos['suscripcion'],PDO::PARAM_STR);
		$stmt->bindParam(":verificacion",$datos['verificacion'],PDO::PARAM_STR);
		$stmt->bindParam(":encriptar_Email",$datos['encriptar_Email'],PDO::PARAM_STR);
		$stmt->bindParam(":patrocinador",$datos['patrocinador'],PDO::PARAM_STR);
		//almacenamos los datos que vamos a enviar.


		if($stmt->execute()){
			return "ok";
		}else{
			//error de la conexion pdo (como mysqli_conect)
			return print_r(conexion::conectar()->errorInfo()); 
		}

		//seguridad

		$stmt->close();
		$stmt=null;
	}
/*=============================================
mostrar usuario
=============================================*/
	static public function mdlMostrarUsuarios($tabla,$item,$valor){

		if($item != null & $valor != null){
			$smt=Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
			//vu¡inculo el parametro con nombre de la variable resultado
			$smt->bindParam(":".$item,$valor,PDO::PARAM_STR);
			$smt->execute();		
			//retorna una sola fila
			return $smt -> fetch();
		
		}else{
			$smt=Conexion::conectar()->prepare("SELECT * FROM $tabla");
			$smt->execute();		
			//retorna una sola fila
			$data['recordsTotal']=4;
			$data['recordsFiltered']=4;
			$data["data"]=$smt -> fetchALL(PDO::FETCH_ASSOC);
			return $data;

		}
		

		$smt->close();
		$smt=null;
	}

/*=============================================
Actualizar Usuario
=============================================*/

	static public function mdlActualizarUsuario($id,$item,$valor,$tabla){

		$smt=Conexion::conectar()->prepare("UPDATE $tabla SET $item=:$item where 
			id_usuario=:id");

			$smt->bindParam(":".$item,$valor,PDO::PARAM_STR);
			$smt->bindParam(":id",$id,PDO::PARAM_STR);

			if($smt->execute()){
				return 'ok';
			}else{
				return print_r(conexion::conectar()->errorInfo()); 
			}


		$smt->close();
		$smt=null;
	}

/*=============================================
iniciar Suscripcion
=============================================*/
	static public function mdlIniciarSuscripcion($tabla,$datos){
		$smt=Conexion::conectar()->prepare("UPDATE $tabla SET suscripcion=:suscripcion,
			id_suscripcion=:id_suscripcion,
			patrocinador=:patrocinador,
			pais=:pais,
			enlace_afiliado=:enlace_afiliado,
			cod_pais=:cod_pais,
			telefono_movil=:telefono_movil,
			ciclo_pago=:ciclo_pago,
			vencimiento=:vencimiento,
			fecha_contrato=:fecha_contrato,
			firma=:firma,
			paypal=:paypal WHERE id_usuario=:id_usuario");

		//print_r($smt);return;

		$smt->bindParam(":suscripcion",$datos['suscripcion'],PDO::PARAM_STR);
		$smt->bindParam(":id_suscripcion",$datos['id_suscripcion'],PDO::PARAM_STR);
		$smt->bindParam(":patrocinador",$datos['patrocinador'],PDO::PARAM_STR);
		$smt->bindParam(":pais",$datos['pais'],PDO::PARAM_STR);
		$smt->bindParam(":enlace_afiliado",$datos['enlace_afiliado'],PDO::PARAM_STR);
		$smt->bindParam(":cod_pais",$datos['cod_pais'],PDO::PARAM_STR);
		$smt->bindParam(":telefono_movil",$datos['telefono_movil'],PDO::PARAM_STR);
		$smt->bindParam(":ciclo_pago",$datos['ciclo_pago'],PDO::PARAM_STR);
		$smt->bindParam(":vencimiento",$datos['vencimiento'],PDO::PARAM_STR);
		$smt->bindParam(":fecha_contrato",$datos['fecha_contrato'],PDO::PARAM_STR);
		$smt->bindParam(":firma",$datos['firma'],PDO::PARAM_STR);
		$smt->bindParam(":paypal",$datos['paypal'],PDO::PARAM_STR);
		$smt->bindParam(":id_usuario",$datos['id_usuario'],PDO::PARAM_INT);

		if($smt->execute()){
			return 'ok';
		}else{
			return print_r(conexion::conectar()->errorInfo()); 
		}
		$smt->close();
		$smt=null;

	}



	static public function mdlCancelarSuscripcion($tabla,$datos){
		$smt = Conexion::conectar()->prepare("UPDATE $tabla set suscripcion=:suscripcion, ciclo_pago=:ciclo_pago, firma=:firma, fecha_contrato=:fecha_contrato WHERE id_usuario=:id_usuario");

		$smt->bindParam(":suscripcion",$datos['suscripcion'],PDO::PARAM_STR);
		$smt->bindParam(":ciclo_pago",$datos['ciclo_pago'],PDO::PARAM_STR);
		$smt->bindParam(":firma",$datos['firma'],PDO::PARAM_STR);
		$smt->bindParam(":fecha_contrato",$datos['fecha_contrato'],PDO::PARAM_STR);
		$smt->bindParam(":id_usuario",$datos['id_usuario'],PDO::PARAM_INT);
		
		if($smt->execute()){
			return "ok";
		}else{

			echo "\nPDO::errorInfo():\	n";
			return print_r(conexion::conectar()->errorInfo()); 
		}

		$smt->close();
		$smt= null;
	}

}
?>