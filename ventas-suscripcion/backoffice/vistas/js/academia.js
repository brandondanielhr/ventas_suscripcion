/*=============================================
MOSTRAR- OCULTAR BOTONES VIDEOS MENU DERECHO
=============================================*/

var toggle= false;


$(".videos .visorVideos .fa-bars").click(function(){
	if(!toggle){
		
		toggle= true;
	
	}else{
		toggle= false;
	}

	ocultarBotones(toggle);

});


function ocultarBotones(toggle){
	if(!toggle){
		//togle:jquery alterna entre hide y show:js
		$(".videos .botonesVideos").toggle("fast");

		$(".videos .visorVideos h5").toggle("fast");
		
		if(window.matchMedia("(max-width:768px)").matches){
			
			$(".videos .visorVideos").css({"width":"70%"});

		}else{
			
			$(".videos .visorVideos").css({"width":"75%"});
		}

	}else{
		$(".videos .visorVideos h5").toggle("fast");
		$(".videos .botonesVideos").toggle("fast");
		$(".videos .visorVideos").css({"width":"100%"});
	}

}
//vid = condicion para evitar error en el ended y que se ejecute en otras paginas 
var vid= $(".videos video").attr("vid");
/*=============================================
Reproducir siguiente video automaticamente
=============================================*/

if(vid){
	setInterval(function(){
		if($(".videos video")[0].ended){

			var ruta_categoria= $(".videos video").attr("ruta_categoria");
			var nex_video = $(".videos video").attr("nex_video");
			
			window.location ="index.php?pagina="+ruta_categoria+"&video="+nex_video;	
		}
	},1000);

}
		
/*=============================================
Velocidad Video
=============================================*/


//en jquey no funciona
//var video2 = $(".videos video");
//console.log(video2[0]);

//en js nativo
var video = document.getElementById('video');
$(".valocidadVideo a").click(function(e){

	e.preventDefault();

	video.playbackRate = $(this).attr("vel");

	$(".valocidadVideo a").removeClass("active");

	$(this).addClass("active");	


});


/*=============================================
Reproduccion hls
=============================================*/

/*var videoSrcHls =$(".visorVideos")attr("rutaVideo");

if(Hls.isSupported()) {
  var hls = new Hls();
  hls.loadSource(videoSrcHls);
  hls.attachMedia(video);
  hls.on(Hls.Events.MANIFEST_PARSED,function() {
    video.play();
  });
}*/


/*=============================================
Sombrear Item video
=============================================*/

var numeroClase=$(".visorVideos").attr("numeroClase");

$(".botonesVideos ul li[numeroClase='"+numeroClase+"']").css({"background":"#ddd","color":"#000"});


