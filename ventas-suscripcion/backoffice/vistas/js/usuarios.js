

$.ajax({

	url:"vistas/js/plugins/paises.json",
	type:"GET",
	success:function(respuesta){

		/*	
		respuesta.forEach(seleccionaPais);
			function seleccionaPais(item){
				var pais= item.name;
				var CodPais= item.code;
				var dial= item.dial_code;
				$("#inputPais").append(
					`<option value="`+pais+','+CodPais+','+dial+`">`+pais+`</option>`
				)
			}
		*/


		// esto hace lo mismo
		respuesta.forEach(function seleccionaPais(item,index,array){

	
			//console.log("item",item.name,'index:',index);
				
				var pais= item.name;
				var CodPais= item.code;
				var dial= item.dial_code;


				$("#inputPais").append(
				
					`<option value="`+pais+','+CodPais+','+dial+`">`+pais+`</option>`


			)	
		})
	}
})


$('.select2').select2()

/*
agregar code del pais 
*/


$('#inputPais').change(function(){
//split tranformar el vlor del imput pais en un array y le dice que todo lo que encuente con coma lo transforme en indices de array	
	$(".dialCode").html($(this).val().split(',')[2])

})

/*
input mask telefono
*/
$('[data-mask]').inputmask();

/*
firma virtual
*/
	
$("#signatureparent").jSignature({

	color:"#33",//color de linea
	lineWidth:1,//grosor
	idth:320,//ancho y largo
	height:100

});

$(".repetirFirma").click(function(){
	
	$("#signatureparent").jSignature("reset");
	

});



function crearCookies(nombre,valor,diasExpiracion){
	var hoy= new Date();

	hoy.setTime(hoy.getTime() + (diasExpiracion*24*60*60*1000));

	var fechaExpiracion = "expires="+ hoy.toUTCString();

	document.cookie= nombre +"="+ valor+ "; "+fechaExpiracion;
}


/*
btn suscribirse(validacion)
*/

$(".suscribirse").click(function(){

	$(".alert").remove();

	var nombre = $("#inputName").val();
	
	var email = $("#inputEmail").val();
	
	var patrocinador = $("#inputPatrocinador").val();
	
	var enlace_afiliado = $("#inputAfiliado").val();
	
	var pais = $("#inputPais").val().split(",")[0];

	var cod_pais = $("#inputPais").val().split(",")[1];

	var telefono_pais = $("#inputPais").val().split(",")[2]+" "+$("#inputMovil").val();

	var type_red = $("#tipoRed").val();
	
	//la propiedad checked verifica si esta chekeado podemos almacenarlo en la var
	var aceptar_terminos= $("#aceptarTerminos:checked").val();
	
	if($("#signatureparent").jSignature("isModified")){
		var firma= $("#signatureparent").jSignature("getData","image/svg+xml");
	
	}



/*---------------------validaciones-----------------*/

	if(nombre == "" ||
		email == ""||
		patrocinador == "" ||
		enlace_afiliado == "" ||
		pais == "" ||
		cod_pais == "" ||
		telefono_pais =="" ||
		type_red == "" ||
		aceptar_terminos != "on" ||
		!$("#signatureparent").jSignature('isModified')){

		$(".suscribirse").before(`
			<div class="alert alert-danger">Falta rellenar datos,no ha aceptado o no ha firmado 
			los terminos y condiciones</div>
			`);
		
		return;
	

	}else{
		$(".suscribirse").before(`
			<div class="alert alert-success">Se completo exitosamente el registro</div>
			`);
			crearCookies("enlace_afiliado",enlace_afiliado,2);
			crearCookies("patrocinador",patrocinador,2);
			crearCookies("pais",pais,2);
			crearCookies("cod_pais",cod_pais,2);
			crearCookies("telefono",telefono_pais,2);
			//guardamos la firma e el indice 1 para poder imprimir el svg en el contrato del pdf
			crearCookies("firma",firma[1],2);
			crearCookies("type_red",type_red,2);



			var datos = new FormData();
			datos.append("suscripcion","ok");
			datos.append("nombre",nombre);
			datos.append("email",email);
		$.ajax({
			url:"ajax/usuarios_ajax.php",
			method: "POST",
			data: datos,
			cache:false,
			contentType:false,
			processData:false,
			//dataType:"json",
			beforeSend:function(){
				$(".suscribirse").after(`

					<img src="vistas/img/plantilla/status.gif"  class="ml-3"	style="Width:30px; height=30px" />
					<span class="alert alert-warning ml-3">Procesando la suscripcion, no cerrar esta pagina</span>				

				`)

			},
			success:function(respuesta){
				window.location=respuesta;
			}

		});
	
	}


});

var hola="prueba";

	


$("#ejemplo").DataTable({
	"scrollX": true,
	"processig":true,
	"serverSide":true,
	"ajax":{
		"url":"http://Academyoflife.com/backoffice/ajax/tabla_aajax.php",
		"type":"POST"				
	},

	"columns":[
	{"data":"id_usuario"},
	{"data":"foto","aTargets": [0],
	"render": function (data){
				if(data == null){
					return "<img src='vistas/img/usuarios/default/default.png' class='img-fluid' Width='30px'/>";
				}else{
                    return "<img src='" +data+ "'class='img-fluid rounded-circle' Width='40px'/>";
				}
    }},
	{"data":"email"},
	{"data":"suscripcion",
	"render": function (data){
				if(data == 0){
					return "<button type='button' class='btn btn-danger btn-st'>Desactivado</button>";
				}else{
                    return "<button type='button' class='btn btn-success btn-st'>Activado</button>";
				}
	}},
	{"data":"id_suscripcion"},
	{"data":"patrocinador"},
	{"data":"enlace_afiliado",
	"render": function (data){
		return "http://Academyoflife.com/"+data;
	}},
	{"data":"paypal"},
	{"data":"pais"}
	]	
});

/*--------------------copiar portapapeles-----------------*/
$(".copiarLink").click(function(){

	var temporal= $("<input>");

	$("body").append(temporal);

	temporal.val($("#linkAfiliado").val()).select();

	document.execCommand("copy");

	temporal.remove();

	$(this).parent().parent().after(`
		<div class="text-muted copiado">El enlace fue copiado en el portapapeles</div>`);

		setTimeout(function(){

			$(".copiado").remove(); 

		},2000)	




})

$(".cancelarSuscripcion").click(function(){

	id_usuario=$(this).attr("idUsuario");
	id_suscripcion=$(this).attr("idSuscripcion");
	ruta=$(this).attr("ruta");
	
	

	swal({
		type:"warning",
		title:"¿Estas seguro de querer cancelar su suscripcion?",
		text:"Si no lo esta puede cancelar la accion, recuerde que perdera todo el trabajo que ha hecho con la red pero recibira el pago de su ultimo mes",
		showCancelButton: true,
	    confirmButtonColor: '#3085d6',
	    cancelButtonColor: '#d33',
	    cancelButtonText: 'Cancelar',
	    confirmButtonText: 'Si, cancelar suscripción!'
	}).then(function(result){

		if(result.value){
			
			var token=null;
			
			var settings1 = {


			"url": "https://api.sandbox.paypal.com/v1/oauth2/token",
			"method": "POST",
			"timeout": 0,
			"headers": {
				"Authorization": "Basic QVM0ZWRxZktwZ25hTjJEbk50X3Y1bU5NVFVfMVpFaEZ0S2Q2YTRfNjVVRkttdHlZUHl0TXNzekRlQXdnYXQ2NGRSbWpLSmFVSGRXcjZYQm86RUJjbFZPam91RGl4Wi1qMjlUWWtrQWRGOWVFN1JzWUtGYkdXMExjUnJrUHVUNGM3WS14UVhtOEdLaVFUeTN6aVlDNXdGYXdFZUYtWk9zQlY=",
				"Content-Type": "application/x-www-form-urlencoded"
				    
			},
				"data": {
				"grant_type": "client_credentials"
				}
			};
			$.ajax(settings1).done(function(response){
			  
				token = "Bearer " + response["access_token"];

				//var res_token=token.substring(6,103);			
				var settings2 = {
				"url": "https://api.sandbox.paypal.com/v1/billing/subscriptions/"+id_suscripcion+"/cancel",
				"method": "POST",
				"timeout": 0,
				"headers": {
				"content-type": "application/json",
				"Authorization":token
				   
				  },
				  "data": "{\r\n  \"reason\": \"Not satisfied with the service\"\r\n}",
				};


				$.ajax(settings2).done(function(response){
					if(response = "undefined"){

						var datos= new FormData();

						datos.append("id_usuario",id_usuario);

						$.ajax({

							url:"ajax/usuarios_ajax.php",
							method: "POST",
							data: datos,
							cache: false,
							contentType: false,
							processData: false,
							success:function(respuesta){
							
								if(respuesta){
									swal({
										type:"success",
									  	title: "¡Su suscripción ha sido cancelada con éxito!",
									  	text: "¡Continua disfrutando de nuestro contenido gratuito!",
									  	showConfirmButton: true,
										confirmButtonText: "Cerrar"
									}).then(function(result){

										if(result.value){   
											window.location = ruta+"backoffice/perfil";
										} 
									});
								}

							}
							
						})
					}
				});

			});
		}
	});

});


/*-------------------------------
Pinterest grid
-------------------------------*/

$(".grid").pinterest_grid({

	no_colums:3,
	padding_X:10,
	padding_y:10,
	margin_bottom:50,
	single_column_breakpoint:700
})































