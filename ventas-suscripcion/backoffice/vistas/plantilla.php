<?php

session_start();
$ruta=ControladorGeneral::ctrRuta();
$valor_suscripcion=ControladorGeneral::ctrValorSuscripcion();
$patrocinador=ControladorGeneral::ctrPatrocinador();

if(!isset($_SESSION['validar'])){
	echo'<script>
	
	window.location="'.$ruta.'ingreso";

	</script>';
	return;
		 
}



$item='id_usuario';
$valor= $_SESSION['id'];
//capturo todos los datos de usuario para poder implementarlos en el backoffice
$usuario=ControladorUsuarios::ctrMostrarUsuarios($item,$valor);
//echo '<pre>'; print_r($usuario); echo '</pre>';	



?>

<!DOCTYPE html>
<html>

<head>
  
  	<meta charset="utf-8">
  
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  	<title>Backoffice| Ventas por suscripcion</title>
  	
  	<link rel="icon" href="vistas/img/plantilla/icono.png">

  	<!-- Tell the browser to be responsive to screen width -->
  	<meta name="viewport" content="width=device-width, initial-scale=1">
		

	<!-------------------------------------------------------------------
	vinculo css
	---------------------------------------------------------------->			

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<!-- Font Awesome -->
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
  	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
  	crossorigin="anonymous">
  	<!-- Google Font: Source Sans Pro -->
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="vistas/css/plugins/adminlte.min.css">
  	
  	<!-- overlayScrollbars -->
  	<link rel="stylesheet" href="vistas/css/plugins/OverlayScrollbars.min.css">

  	<!-- jdSlider -->
	<link rel="stylesheet" href="vistas/css/plugins/jdSlider.css">

	<!-- Select2 -->
	<link rel="stylesheet" href="vistas/css/plugins/select2.min.css">

  	<!-- Estilo personalizado -->
  	<link rel="stylesheet" href="vistas/css/plugins/JdSlider.css">

  	<!-- Estilo personalizado -->
  	<link rel="stylesheet" href="vistas/css/style.css">

	<!-- DataTables -->
	<link rel="stylesheet" href="vistas/css/plugins/dataTables.bootstrap4.min.css">	
	<link rel="stylesheet" href="vistas/css/plugins/responsive.bootstrap.min.css">

  	<!-------------------------------------------------------------------
	vinculo js
	---------------------------------------------------------------->
	<!-- Jquery-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

	<!-- AdminLTE App -->
	<script src="vistas/js/plugins/adminlte.min.js"></script>
	
	<!-- overlayScrollbars -->
	<script src="vistas/js/plugins/jquery.overlayScrollbars.min.js"></script>

	<!-- jdSlider -->
	<!-- https://www.jqueryscript.net/slider/Carousel-Slideshow-jdSlider.html -->
	<script src="vistas/js/plugins/JdSlider.js"></script>

	<!-- Select2 -->	
	<!-- https://github.com/select2/select2 -->
	<script src="vistas/js/plugins/select2.full.min.js"></script>

	<script src="vistas/js/plugins/sweetalert2.all.js"></script>

	<!-- file:///C:/Users/chechi/Downloads/AdminLTE-3.0.0/AdminLTE-3.0.0/plugins/inputmask/min/jquery.inputmask.bundle.min.js-->
	<script src="vistas/js/plugins/jquery.inputmask.bundle.min.js"></script>

	<!-- jSignature -->
	<!-- https://www.jqueryscript.net/other/Signature-Field-Plugin-jQuery-jSignature.html -->
	<script src="vistas/js/plugins/jSignature.js"></script>
	<script src="vistas/js/plugins/jSignature.CompressorSVG.js"></script>

	<!-- DataTables 
	https://datatables.net/-->
  	<script src="vistas/js/plugins/jquery.dataTables.min.js"></script>
  	<script src="vistas/js/plugins/dataTables.bootstrap4.min.js"></script> 
	<script src="vistas/js/plugins/dataTables.responsive.min.js"></script>
  	<script src="vistas/js/plugins/responsive.bootstrap.min.js"></script>


  	<!-- HLS -->
	<!-- https://poanchen.github.io/blog/2016/11/17/how-to-play-mp4-video-using-hls -->
  	<script src="vistas/js/plugins/hls.min.js"></script>	

  
	<!-- Pinterest Grid -->	
	<!-- https://www.jqueryscript.net/layout/Simple-jQuery-Plugin-To-Create-Pinterest-Style-Grid-Layout-Pinterest-Grid.html -->
	<script src="vistas/js/plugins/pinterest_grid.js"></script>



</head>  


<body class="hold-transition sidebar-mini sidebar-collapse">

<div class="wrapper">

<?php

/*siempre estan presentes*/
include"paginas/modulos/cabezera.php";

include"paginas/modulos/menu_lateral.php";

/*paginas dinamicas*/
if(isset($_GET['pagina'])){

	//hacer dinamicas las url de academy analizando

	/*$categorias = ControladorAcademia::ctrMostrarCategorias(null, null);
	$paginaAcademia = null;

	foreach ($categorias as $key => $value) {
		
		if( $_GET["pagina"] == $value["ruta_categoria"]){

			$paginaAcademia = $value["ruta_categoria"];

		}
	}*/

	if($_GET['pagina']=='inicio' || 
		$_GET['pagina']=='perfil'||
		$_GET['pagina']=='usuarios'||
		$_GET['pagina']=='uninivel'||
		$_GET['pagina']=='binaria'||
		$_GET['pagina']=='matriz'||
		$_GET['pagina']=='ingresos-uninivel'||
		$_GET['pagina']=='ingresos-binario'||
		$_GET['pagina']=='ingresos-matriz'||
		$_GET['pagina']=='plan-compensacion'||
		$_GET['pagina']=='soporte'||
		$_GET['pagina']=='salir'){

		include"paginas/".$_GET['pagina'].".php";
	}

	else if($_GET['pagina']=='modelados'||
		$_GET['pagina']=='animaciones'||
		$_GET['pagina']=='paisajes'){

		include"paginas/academia.php";
	}


	else{
		include"paginas/error404.php";
	}

}else{
	include"paginas/inicio.php";
}

include"paginas/modulos/footer.php";





?>


</div>
<script src="vistas/js/menu.js"></script>
<script src="vistas/js/inicio.js"></script>	
<script src="vistas/js/usuarios.js"></script>	
<script src="vistas/js/academia.js"></script>	
<script src="vistas/js/multinivel.js"></script>	

</body>

</html>