  <div class="content-wrapper" >
      <!-- Content Header (Page header) -->
      <section class="content-header">
      
        <div class="container-fluid">
         
          <div class="row mb-2">
           
            <div class="col-sm-6">
              <h1>Academia</h1>
           
            </div>
            
            <div class="col-sm-6">
            
              <ol class="breadcrumb float-sm-right">
               
                <li class="breadcrumb-item"><a href="inicio">Academia</a></li>
                
                <li class="breadcrumb-item active">Mi perfil</li>
             
              </ol>
           
            </div>
         
          </div>
       
        </div><!-- /.container-fluid -->
     
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="container-fluid">
          <!-- escondame la info en pantallas pequeñas en grandes si se muestre las vistas previas -->
          <div class="d-none d-md-block">

          <?php

            include "modulos/inicio/vistas-previas.php";
          
           ?>
          </div>
          
          <?php
            include "modulos/academia/videos.php";
          ?>


        </div>

      </section>
      <!-- /.content -->
  </div>