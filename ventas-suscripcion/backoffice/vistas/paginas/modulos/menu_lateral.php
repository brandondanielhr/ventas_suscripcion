<div> 

  <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="inicio" class="brand-link">
        <img src="vistas/img/plantilla/icono.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Academy of life</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
           <?php if($usuario['foto']==""):?>

            <img class="rounded-circle" src="vistas/img/usuarios/default/default.png">   
          
            <?php else: ?>
            
            <img class="rounded-circle" src="<?php echo $usuario["foto"] ?>">  

            <?php endif?>

          </div>
          <div class="info">
            <a href="perfil" class="d-block"><?php echo $usuario['nombre']?></a>
          </div>
        </div>

        <!-- Sidebar -->
        <nav class="mt-3">
          
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            <!--=========================
            =         Boton Inicio      =   
            ===========================-->


            <li class="nav-item"></li>
              <a href="inicio" class="nav-link">
                  
                  <i class="nav-icon fas fa-home"></i>
                  <p>Inicio</p>
                </a>
            </li>  

            <!--=========================
            =       Boton Mi Perfil     =   
            ===========================-->

            <li class="nav-item"></li>
              <a href="perfil" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>Mi perfil</p>
              </a>
            </li>  

            <!--=========================
            =    Boton Usuarios         =   
            ===========================-->

            <?php if($usuario['perfil']=='admin'): ?>
            <li class="nav-item "></li>
              <a href="usuarios" class="nav-link tabla_hola">
                <i class="nav-icon fas fa-users"></i>
                <p>Usuarios</p>
              </a>
            </li> 
            <?php endif ?>

            <!--=========================
            =  Boton academia           =   
            ===========================-->
            <li class="nav-item has-treeview">
             
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-graduation-cap"></i>
                <p>
                Academia
                <i class="right fas fa-angle-left"></i>
                </p>
              </a>
             
              <ul class="nav nav-treeview  btn-academia">
      

                
              </ul>
            
            </li> 
            <!--=========================
            =  Boton Mi red             =   
            ===========================-->

            <li class="nav-item has-treeview">
             
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-sitemap"></i>
                <p>
                Mi red
                <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="uninivel" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Red uninivel</p>
                  </a>
                </li>
              
                <li class="nav-item">
                  <a href="binaria" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Red binaria</p>
                  </a>
                </li>
              
                <li class="nav-item">
                  <a href="matriz" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Red matriz 4x4</p>
                  </a>
                </li>
              </ul>
            
            </li> 
            <!--=========================
            =  Boton Ingresos           =   
            ===========================-->
            
            <li class="nav-item has-treeview">
             
              <a href="#" class="nav-link">
                <!-- fontawesome para iconos -->
                <i class="nav-icon fas fa-money-check-alt"></i>
                <p>
                Ingresos 
                <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="ingresos-uninivel" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ingresos uninivel</p>
                  </a>
                </li>
              
                <li class="nav-item">
                  <a href="ingresos-binario" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ingresos binaria</p>
                  </a>
                </li>
              
                <li class="nav-item">
                  <a href="ingresos-matriz" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ingresos matriz 4x4</p>
                  </a>
                </li>
              </ul>
            </li>  
            
            <!--=========================
            =  Plan de Compensacion     =   
            ===========================-->

            <li class="nav-item"></li>
              <a href="plan-compensacion" class="nav-link">
                <i class="nav-icon fas fa-gem"></i>
                <p>Plan de compensacion</p>
              </a>
            </li> 

            <!--=========================
            = Soporte                   =   
            ===========================-->


            <li class="nav-item"></li>
              <a href="soporte" class="nav-link">
                <i class="nav-icon fas fa-comments"></i>
                <p>Soporte</p>
              </a>
            </li> 
            <!--=========================
            =  Cerrar Sesion            =   
            ===========================-->

            <li class="nav-item"></li>
              <a href="salir" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>Cerrar sesion</p>
              </a>
            </li>            
            <!--=========================
            =  Boton academia           =   
            ===========================-->

          </ul>    
        </nav>
      </div> 
    <aside>     
</div>


