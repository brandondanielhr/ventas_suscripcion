<?php

	/*=============================================
	traer videos de acuerdo a la categoria
	=============================================*/

if(isset($_GET['pagina'])){

	$item="ruta_categoria";
	$valor=$_GET["pagina"];


	$academia = ControladorAcademia::ctrMostrarAcademia($item,$valor);
}
/*=============================================
traer video
=============================================*/
if(isset($_GET['video'])){

	$item="id_video";
	$valor=$_GET["video"];
	
	$video = ControladorAcademia::ctrMostrarVideo($item,$valor);



	if($video["vista_gratuita"] == 0 && $usuario["suscripcion"] == 0 ){
		echo '<script>
			window.location="'.$ruta.'backoffice/perfil"

			</script>';
			return;
	}

	$idVideo=$video['id_video'];
	$poster=$video['imagen_video'];
	$rutaVideo=$video['medios_video'];
	$titulo=$video['titulo_video'];


}else{

	$idVideo=$academia[0]['id_video'];
	$poster=$academia[0]['imagen_video'];
	$rutaVideo=$academia[0]['medios_video'];
	$titulo=$academia[0]['titulo_video'];

}


$numero_clase=0;
foreach ($academia as $key => $value) {
	++$numero_clase;
	if ($value["id_video"] == $idVideo) {
		break;
	}

}
/*=============================================
Reproducir siguiente video automatico 
=============================================*/


if(empty($academia[$numero_clase]["id_video"])){

	$nextVideo=$academia[0]["id_video"];

}else{

	$nextVideo=$academia[$numero_clase]["id_video"];

}





?>


<div class="card card-default color-palette-box videos">
	
	<div class="visorVideos" numeroClase="<?php echo ($key+1)?>">

		<h5 class="text-white p-2 p-md-3 text-light w-100 d-flex"><?php echo 
		$numero_clase.". ". $titulo ?>
			
			<div class="valocidadVideo dropdown ml-2">
				<div data-toggle="dropdown">
					<i class="fas fa-tachometer-alt"></i>
			
				</div>	
				<div class="dropdown-menu">
					<a class="dropdown-item" href="#" vel=".25">0.25x</a>
					<a class="dropdown-item" href="#" vel=".50">0.5x</a>
					<a class="dropdown-item active" href="#" vel="1">1x</a>
					<a class="dropdown-item" href="#" vel="1.50">1.5x</a>
					<a class="dropdown-item" href="#" vel="2">2x</a>	
				</div>
			</div>

		</h5>
		
		<video id="video" controls poster="<?php echo $poster; ?>" 
			ruta_categoria="<?php echo $_GET["pagina"] ;?>" nex_video="<?php 
										echo $nextVideo ;?>" vid="hola">
			
			<source src="<?php echo $rutaVideo; ?>" type="video/mp4" class="video1">

		</video>

		<i class="fas fa-bars"></i>


	</div>
	
	
	<div class="botonesVideos">



		<ul class="list-group list-group-flush">
			
			<?php foreach ($academia as $key => $value): ?>
				
				<?php if($usuario["suscripcion"] == 1):?>	
			
					<li class="list-group item list-group-item-action py-3 px-2 d-flex" numeroClase="<?php echo ($key+1)?>" >
						
						<img src="<?php echo $value["imagen_video"]?>" class="img-thumbnail p-0 mr-2">
						
						<a href="index.php?pagina=<?php 
										echo $value["ruta_categoria"] ;?>&video=<?php echo $value["id_video"];?>" class="text-muted">
							<?php echo ($key+1).". ".$value["titulo_video"]?>
							
						</a>		

					</li>

				<?php else: ?>

					<?php if($value["vista_gratuita"] == 1):?>

						<li class="list-group item list-group-item-action py-3 px-2 d-flex" numeroClase="<?php echo ($key+1)?>">
							
							<img src="<?php echo $value["imagen_video"]?>" class="img-thumbnail p-0 mr-2">
							
							<a href="index.php?pagina=<?php 
										echo $value["ruta_categoria"];?>&video=<?php echo $value["id_video"];?>" class="text-muted">
							<?php echo ($key+1).". ".$value["titulo_video"]?>
							
							</a>	

						</li>
					
					<?php endif ?>		
				
				<?php endif ?>

		
			<?php endforeach ?>	
		</ul>
		
	

	</div>


</div>



