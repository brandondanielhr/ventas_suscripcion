<div class="col-12 col-lg-7">

	<div class="card card-brown card-outline">
		
		<div class="card-header">
			
			<h5 class="m-0 float-left"><b>Mi red:12 activos -3 desactivados</b></h5>


			<h5 class="float-right"><b>Patrocinador:</b>
				<span class="badge badge-secondary">Academy-Of-Life</span>
			</h5>
		
		</div>

		<div class="card-body">
			
			<table class="table table-bordered table-striped dt-responsive tablaUninivel" width="100%">
				<thead>
	                <tr>
	                	<th style="width:10px">#</th>
	                	<th>Foto</th>
	                	<th>Nombres</th>
	                	<th>Pais</th>
	                	<th>Vencimiento</th>
	                	<th>Patrocinador</th>
	                	<th>Suscripcion</th>
	                </tr>
	            </thead>

	            <tbody id="nombre_tabla">

	            	
	            	
	            </tbody>
				
			</table>

		</div>	

	</div>	

</div>