<div class="row">
	<!--------------
	Primera etiqueta
	--------------->
	<div class="col-12 col-sm-6 col-lg-3">
		
		<div class="small-box bg-green">

			<div class="inner">
				
				<h3>$ monto</h3>

				<p class="text-uppercase"><h6>comisiones de este periodo</h6></p>
			
			</div>

			<div class="icon">
				
				<i class="fas fa-dollar-sign"></i>

			</div>

			<a href="" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>

		</div>

	</div>



	<!--------------
	Segunda etiqueta
	--------------->


	<div class="col-12 col-sm-6 col-lg-3">
		
		<div class="small-box bg-yellow">

			<div class="inner">
				
				<h3>$<?php echo number_format(70,2,",",".")?></h3>

				<p class="text-uppercase"><h6>Ventas de este periodo</h6></p>
			</div>

			<div class="icon">
				
				<i class="fas fa-wallet"></i>

			</div>

			<a href="" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>

		</div>

	</div>



	<!--------------
	Tercera etiqueta
	--------------->

	<div class="col-12 col-sm-6 col-lg-3">
		
		<div class="small-box bg-blue">

			<div class="inner">
				
				<h3>0</h3>

				<p class="text-uppercase"><h6>Mis Tickets</h6></p>
			</div>

			<div class="icon">
				
				<i class="fas fa-comments"></i>

			</div>

			<a href="" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>

		</div>

	</div>

	<!--------------
	cuarta etiqueta
	--------------->

	<div class="col-12 col-sm-6 col-lg-3">
		
		<div class="small-box bg-red">

			<div class="inner">
				
				<h3 class="text-dark">2019-10-29</h3>

				<p class="text-uppercase"><h6 atr="prueba" id="color_black">Proximo pago de comision</h6></p>
			</div>

			<div class="icon">
				
				<i class="fas fa-user-plus"></i>

			</div>

			<a href="" class="small-box-footer">Cancelar Suscripcion<i class="fa fa-arrow-circle-right"></i></a>

		</div>
			



	</div>

</div>