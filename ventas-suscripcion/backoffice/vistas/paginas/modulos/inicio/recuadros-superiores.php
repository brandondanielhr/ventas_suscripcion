
<div class="row">
  
  <div class="col-12 col-sm-6 col-lg-3">
      <!-- small box -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3>$150</h3>

        <p>Mis comisiones</p>
      </div>
      
      <div class="icon">
        <i class="fas fa-dollar-sign"></i>
        </div>
        <a href="ingresos-uninivel" class="small-box-footer">mas informacion<i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
      


      <!-- ./col -->
  <div class="col-12 col-sm-6 col-lg-3">
    <!-- small box -->
    <div class="small-box bg-purple">
      <div class="inner">
        <h3>53</h3>

        <p>Mi red</p>
      </div>
      
      <div class="icon">
        <i class="fas fa-sitemap"></i>
      </div>
      <a href="uninivel" class="small-box-footer">Mas informacion<i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
      


      <!-- ./col -->
  <div class="col-12 col-sm-6 col-lg-3">
    <!-- small box -->
    <div class="small-box bg-primary">
      
      <div class="inner">
        <h3>4</h3>

        <p>Mis tickets</p>
      </div>
      
      <div class="icon">
        <i class="fas fa-comments"></i>
      </div>
      <a href="soporte" class="small-box-footer">Mas informacion<i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
      

      <!-- ./col -->
  <div class="col-12 col-sm-6 col-lg-3">
      <!-- small box -->
    <div class="small-box bg-dark">
      <div class="inner">
        <h3 >Activo</h3>

        <p>Renovacion 2020-03-05</p>
      </div>
      
      <div class="icon">
        <i class="fas fa-user-plus"></i>
      </div>
      <a href="perfil" class="small-box-footer">Cancelar suscripcion<i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>


</div>