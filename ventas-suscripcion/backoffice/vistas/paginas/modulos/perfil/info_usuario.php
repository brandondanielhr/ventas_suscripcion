<div class="col-12 col-md-4">
	
	<div class="card card-info card-outline">

		<div class="card-body box-profile">

			<div class="text-center">
				
				<?php if($usuario['foto']==""):?>

				<img class="profile-user-img img-fluid img-circle" src="vistas/img/usuarios/default/default.png">		
			
				<?php else: ?>
				
				<img class="profile-user-img img-fluid img-circle" src="<?php echo $usuario["foto"] ?>">	

				<?php endif?>




			</div>

	
			<h3 class="profile-username text-center">
				<?php echo $usuario['nombre']?>	
			</h3>
			
			<p class="text-muted text-center">
				
				<?php echo $usuario['email']?>	
				
			</p>

			<div class="text-center"><br>
				
				<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#cambiar_foto">Cambiar foto</button>
				<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#cambiar_password">Cambiar contraseña</button><br>
				

			</div>	

		</div>
		

		<div class="card-footer">


			<button class="btn btn-default float-right">Eliminar cuenta</button>

		</div>	
	</div>	

</div>

<!-------------------------------------
Cambiar foto	
---------------------------------------->

<!-- The Modal -->
<div class="modal" id="cambiar_foto">
  	
  	<div class="modal-dialog">
   		
   		<div class="modal-content">

	  		<form method="post" enctype="multipart/form-data">

		    	<!-- Modal Header -->
		    	<div class="modal-header">
		    		<h4 class="modal-title">Cambiar Imagen</h4>
		    		<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	</div>

		      	<!-- Modal body -->
		      	<div class="modal-body">
		        	
		      		<input type="hidden" name="id_usuario_foto" value="<?php echo $usuario['id_usuario']?>">

		        	<div class="form-group">

		        		<input type="file" class="form-control-file border" name="cambiar_foto" required>

		        		<input type="hidden" name="foto_actual" value="<?php echo $usuario['foto'] ?>">
		        	
		        	</div>

		      	</div>

			    <!-- Modal footer d-flex... sirve para separar los botones-->
			    <div class="modal-footer d-flex justify-content-between">

			       	<div>
			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					</div>

					<div>
			       		<button type="submit" class="btn btn-primary" >Enviar</button>
					</div>
							

			    </div>


			    <?php



			    $cambiarImagen=new ControladorUsuarios();
			    $cambiarImagen->ctrCambiarImagen();


			    ?>
   			
   			</form>
   		
   		</div>
  	
  	</div>

</div>








<!-------------------------------------
Cambiar Password	
---------------------------------------->

<!-- The Modal -->
<div class="modal" id="cambiar_password">
  	
  	<div class="modal-dialog">
   		
   		<div class="modal-content">

	  		<form method="post" >

		    	<!-- Modal Header -->
		    	<div class="modal-header">
		    		<h4 class="modal-title">Cambiar Contraseña</h4>
		    		<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	</div>

		      	<!-- Modal body -->
		      	<div class="modal-body">
		        	
		      		<input type="hidden" name="id_usuario_password" value="<?php echo $usuario['id_usuario']?>">

		        	<div class="form-group">

		        		<input type="password" class="form-control" placeholder="Nuevo password" 
		        		name="editar_password" required>
		        	
		        	</div>

		      	</div>

			    <!-- Modal footer d-flex... sirve para separar los botones-->
			    <div class="modal-footer d-flex justify-content-between">

			       	<div>
			       		<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					</div>

					<div>
			       		<button type="submit" class="btn btn-primary" >Enviar</button>
					</div>
							

			    </div>


			    <?php



			    $cambiarImagen=new ControladorUsuarios();
			    $cambiarImagen->ctrCambiarPassword();


			    ?>
   			
   			</form>
   		
   		</div>
  	
  	</div>

</div>




