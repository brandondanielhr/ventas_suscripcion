<div class="col-12">
	<div class="card card-purple card-outline">

		<div>
			<h5 class="m-0 text-uppercase text-secondary"><b>Material de Promocion</b></h5>
		</div>
		
	

		<div class="card-body">
			
			<section class="grid">

				<article class="white-panel">
					
					<div class="card p-1">
						
						<img src="vistas/img/promocion/photo1.png" class="card-img-top" alt="Card image">

						<div class="card-img-overlay">
							
							<p class="bg-dark text-white-50 p-2 mb-2">Banner 01 (1250px = 835px)</p>

							<a href="vistas/img/promocion/photo1.png" class="btn btn-primary btn-sm" target="_blank">Descargas</a>
						
						</div>

					</div>

				</article>

				<article class="white-panel">
					
					<div class="card p-1">
						
						<img src="vistas/img/promocion/codigo_limpio.jpg" class="card-img-top" alt="Card image">

						<div class="card-img-overlay">
							
							

							<a href="vistas/img/promocion/codigo_limpio.jpg" class="btn btn-primary btn-sm" target="_blank">Descargas</a>
						
						</div>

					</div>

				</article>

				<article class="white-panel">
					
					<div class="card p-1">
						
						<img src="vistas/img/promocion/hackers.jpg" class="card-img-top" alt="Card image">

						<div class="card-img-overlay">
							
							<p class="bg-dark text-white-50 p-2 mb-2">Hackers white books</p>

							<a href="vistas/img/promocion/hackers.jpg" class="btn btn-primary btn-sm" target="_blank">Descargas</a>
						
						</div>

					</div>

				</article>

				<article class="white-panel">
					
					<div class="card p-1">
						
						<img src="vistas/img/promocion/java-png.png" class="card-img-top" alt="Card image">

						<div class="card-img-overlay">
							
							<p class="bg-dark text-white-50 p-2 mb-2">java-png.png</p>

							<a href="vistas/img/promocion/java-png.png" class="btn btn-primary btn-sm" target="_blank">Descargas</a>
						
						</div>

					</div>

				</article>

				<article class="white-panel">
					
					<div class="card p-1">
						
						<img src="vistas/img/promocion/photo4.jpg" class="card-img-top" alt="Card image">

						<div class="card-img-overlay">
							
							<p class="bg-dark text-white-50 p-2 mb-2">Banner 01 (1250px = 835px)</p>

							<a href="vistas/img/promocion/photo4.jpg" class="btn btn-primary btn-sm" target="_blank">Descargas</a>
						
						</div>

					</div>

				</article>


				
			</section>

		</div>

	</div>
</div>