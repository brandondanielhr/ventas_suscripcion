<div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
    
      <div class="container-fluid">
       
        <div class="row mb-2">
         
          <div class="col-sm-6">
            <h1>Plan de compensacion</h1>
         
          </div>
          
          <div class="col-sm-6">
          
            <ol class="breadcrumb float-sm-right">
             
              <li class="breadcrumb-item"><a href="inicio">Plan de compensacion</a></li>
              
              <li class="breadcrumb-item active">Tablero</li>
           
            </ol>
         
          </div>
       
        </div>
     
      </div><!-- /.container-fluid -->
   
    </section>

    <!-- Main content -->
    <section class="content">

    <div class="container-fluid">
      
      <?php     
      
      include"modulos/plan-compensacion/diapositivas.php";
      
      include"modulos/plan-compensacion/videos.php";
      ?>


    </div>
    
    </section>
    
</div>