<?php

if($usuario['perfil'] != "admin"){

  echo'<script>
  window.location="'.$ruta.'backoffice/error404";
  </script>';
  return;

}


?>

  <div class="content-wrapper" >
      <!-- Content Header (Page header) -->
      <section class="content-header">
      
        <div class="container-fluid">
         
          <div class="row mb-2">
           
            <div class="col-sm-6">
              <h1>usuarios</h1>
           
            </div>
            
            <div class="col-sm-6">
            
              <ol class="breadcrumb float-sm-right">
               
                <li class="breadcrumb-item"><a href="inicio">Usuarios</a></li>
                
                <li class="breadcrumb-item active">Mi perfil</li>
             
              </ol>
           
            </div>
         
          </div>
       
        </div><!-- /.container-fluid -->
     
      </section>

      <!-- Main content -->
      <section class="content" >

        <!-- Default box -->
       
        <div class="card">
          <div class="card-header" >
         
            <h3 class="card-title">Usuarios Registrados</h3>

            <div class="card-tools">
             
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
               
                <i class="fas fa-minus"></i></button>
             
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            
            </div>
          
          </div>
         
          <div class="card-body tabla">
            <table class="display nowrap table table-hover table-bordered " id="ejemplo" width="70%" >
              <thead>
                <tr>
                  <th>id_usuario</th>
                  <th>Foto</th>
                  <th>email</th>
                  <th>suscripcion</th>
                  <th>id_suscripcion</th>
                  <th>patrocinador</th>
                  <th>enlace_afiliado</th>
                  <th>paypal</th>
                  <th>pais</th>
                </tr>
              </thead>
            </table>
        
          </div>
          <!-- /.card-body -->
         
          <div class="card-footer">
            Footer
          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->


  </div>

