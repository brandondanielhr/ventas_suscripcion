  <div class="content-wrapper" >
      <!-- Content Header (Page header) -->
      <section class="content-header">
      
        <div class="container-fluid">
         
          <div class="row mb-2">
           
            <div class="col-sm-6">
              <h1>Uninivel</h1>
           
            </div>
            
            <div class="col-sm-6">
            
              <ol class="breadcrumb float-sm-right">
               
                <li class="breadcrumb-item"><a href="inicio">Uninivel</a></li>
                
                <li class="breadcrumb-item active">Mi perfil</li>
             
              </ol>
           
            </div>
         
          </div>
       
        </div><!-- /.container-fluid -->
     
      </section>

      <!-- Main content -->
      <section class="content">

       <div class="container-fluid">
         
          <?php

            include"modulos/uninivel/analitica-uninivel.php";
          ?>
          
          <div class="row">
          <?php
            include"modulos/uninivel/tabla_uninivel.php";
            include"modulos/uninivel/mapa_uninivel.php";
          ?>
        </div>

       </div>



      </section>
      <!-- /.content -->
  </div>