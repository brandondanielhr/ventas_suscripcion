-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-04-2020 a las 00:47:51
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ventas-suscripcion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` text NOT NULL,
  `descripcion_categoria` text NOT NULL,
  `icono_categoria` text NOT NULL,
  `color_categoria` text NOT NULL,
  `fecha_categoria` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ruta_categoria` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `nombre_categoria`, `descripcion_categoria`, `icono_categoria`, `color_categoria`, `fecha_categoria`, `ruta_categoria`) VALUES
(1, 'modelados', '¿Qué es la abstracción? ¿Qué hacemos con ella? ¿Cómo funciona? Bueno\r\nprimero que nada debemos entender que podría ser abstraer fuera del campo de\r\nla programación. La abstracción por si solo como definición es la separación, o la\r\nsustracción de propiedades de un objeto, analizandolas e ignorando lo demás que\r\ncompone a dicho objeto.', 'fas fa-heart', 'purple', '2020-04-16 16:47:17', 'modelados'),
(2, 'animaciones', 'Podemos hablar en abstracción de los Tipos de datos Abstractos que no\r\npertenecen a nuestro lenguaje un ejemplo de esto sería nuestros famosos objetos\r\nlos cuales están constituidos por funcionalidades y características propias los\r\ncuales el programador le define en las clases y no son predefinidas por el\r\nlenguaje.', 'fas fa-puzzle-piece', 'info', '2020-04-16 16:47:40', 'animaciones'),
(3, 'paisajes', 'También es de destacar que la abstracción busca evitar mostrar la implementación\r\nde los tipos de datos, así solo muestra el como lo hace que es lo único que al\r\ncliente le interesa.', 'fas fa-wind ', 'primary', '2020-04-16 16:47:45', 'paisajes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `perfil` text NOT NULL,
  `nombre` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `suscripcion` int(11) NOT NULL,
  `id_suscripcion` text,
  `ciclo_pago` int(11) DEFAULT NULL,
  `vencimiento` date DEFAULT NULL,
  `verificacion` int(11) NOT NULL,
  `email_encriptado` text,
  `foto` text,
  `enlace_afiliado` text,
  `patrocinador` text,
  `paypal` text NOT NULL,
  `pais` text,
  `cod_pais` text,
  `telefono_movil` text,
  `firma` text,
  `fecha_contrato` date DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `perfil`, `nombre`, `email`, `password`, `suscripcion`, `id_suscripcion`, `ciclo_pago`, `vencimiento`, `verificacion`, `email_encriptado`, `foto`, `enlace_afiliado`, `patrocinador`, `paypal`, `pais`, `cod_pais`, `telefono_movil`, `firma`, `fecha_contrato`, `fecha`) VALUES
(1, 'admin', 'academy of line', 'brandondanielhh@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auXBm1Vr2M1NV5t/zNQtGHGpS5fFirrbG', 1, NULL, NULL, NULL, 1, NULL, 'vistas/img/usuarios/1/653.jpg', 'academy-of-life', 'Academy-of-life', 'brandondanielhr@gmail.com', NULL, NULL, NULL, NULL, NULL, '2020-03-17 15:05:42'),
(2, 'usuarios', 'Daniel', 'brandondanielhr@gmail.com', '$2a$07$asxx54ahjppf45sd87a5au5ZbXedrglDH5CPxUjwRaV8RnBB2jysO', 1, 'I-W8B867U8THV3', 1, '2020-04-10', 1, 'acec939d6a603743e9443fc74d683ef4', NULL, 'daniel-2', 'academy-of-life', 'sb-hkl2i1378371@personal.example.com', '0', 'AF', ' 93 (453) 453-4534', 'image/svg xml,<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"160\" height=\"68\"><path stroke-linejoin=\"round\" stroke-linecap=\"round\" stroke-width=\"1\" stroke=\"#33\" fill=\"none\" d=\"M 1 61 c 0.18 -0.04 6.84 -0.91 10 -2 c 6.38 -2.2 12.38 -5.85 19 -8 c 20.42 -6.64 41.68 -11.07 61 -18 c 5.99 -2.15 11.85 -6.29 17 -10 c 2.95 -2.13 5.9 -5.27 8 -8 c 1 -1.3 1.73 -3.37 2 -5 c 0.35 -2.12 0.64 -5.55 0 -7 c -0.41 -0.93 -2.78 -2.07 -4 -2 c -3.64 0.21 -9.14 1.34 -13 3 c -5.07 2.17 -10.2 5.68 -15 9 c -3.9 2.7 -7.85 5.69 -11 9 c -3.01 3.17 -6.57 7.34 -8 11 c -1.27 3.24 -1.34 8.3 -1 12 c 0.29 3.21 1.08 7.87 3 10 c 3.58 3.95 10.53 10.25 16 11 c 20.29 2.77 54.8 1.46 72 1 l 2 -3\"/></svg>', '2020-04-10', '2020-03-24 16:47:06'),
(3, 'usuarios', 'brandon', 'Leonardo@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aucfKA7IzVT/3iOQo4lfhWsxD1bbYDD.6', 1, 'I-888JNFWHG51P', 1, '2020-04-19', 1, '48a44670275f0cd3a80f53f42630f66f', 'vistas/img/usuarios/3/683.png', 'academy-of-life', 'academy-of-life', 'sb-hkl2i1378371@personal.example.com', 'Afghanistan', 'AF', ' 93 (334) 243-2432', '<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"120\" height=\"58\"><path stroke-linejoin=\"round\" stroke-linecap=\"round\" stroke-width=\"1\" stroke=\"#33\" fill=\"none\" d=\"M 2 19 c -0.02 0.16 -1.2 6.42 -1 9 c 0.1 1.28 1.04 3.25 2 4 c 1.63 1.27 4.66 2.79 7 3 c 11.35 1.03 25.52 1.88 37 1 c 4.91 -0.38 10.41 -2.79 15 -5 c 4.83 -2.33 10.38 -5.8 14 -9 c 1.46 -1.29 2.46 -3.95 3 -6 c 1.06 -4.01 2.2 -10.07 2 -13 c -0.05 -0.8 -2.4 -2.37 -3 -2 c -2.37 1.46 -7.73 6.29 -10 10 c -3.67 5.99 -7.04 14.13 -9 21 c -1.21 4.22 -1.81 10.13 -1 14 c 0.67 3.22 3.28 9.49 6 10 c 12.37 2.31 40.37 1.46 53 1 l 2 -3\"/></svg>', '2020-04-19', '2020-03-24 19:08:41'),
(4, 'usuarios', 'jorge gaitan', 'Jorgegaitan@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auruyC1rKJu6lE8ZPTHTKfAzmnBcWK5tC', 0, NULL, 1, '2020-04-10', 1, 'bcd949b48620e934277f6e1b77a1abba', 'vistas/img/usuarios/4/994.jpg', 'jorge-gaitan-4', 'academy-of-life', 'sb-hkl2i1378371@personal.example.com', 'Venezuela', 'Bolivarian Republic of', 'VE (241) 525-001_', 'image/svg xml,<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"289\" height=\"50\"><path stroke-linejoin=\"round\" stroke-linecap=\"round\" stroke-width=\"1\" stroke=\"#33\" fill=\"none\" d=\"M 8 1 c -0.12 0.09 -6.29 3.12 -7 5 c -0.91 2.42 -0.73 8.67 1 11 c 3.89 5.25 12.21 11.7 19 16 c 9.17 5.8 20.06 13.47 30 15 c 21.93 3.37 57.27 3.48 74 1 c 3.34 -0.49 5.89 -8.69 7 -13 c 1.36 -5.26 2.52 -14.95 1 -18 c -0.94 -1.89 -7.5 -2.11 -11 -2 c -6.33 0.2 -13.93 1.08 -20 3 c -6.04 1.91 -12.64 5.65 -18 9 c -2.26 1.41 -4.59 3.99 -6 6 c -0.69 0.98 -1.65 3.86 -1 4 c 6.8 1.46 28.6 4.76 43 5 c 25.64 0.43 51.75 0.26 77 -3 c 26.16 -3.38 54.48 -10.28 78 -17 l 13 -9\"/></svg>', '2020-04-10', '2020-03-30 17:17:47'),
(5, 'usuarios', 'Eduardo', 'Eduardo@gmail.com', '$2a$07$asxx54ahjppf45sd87a5au7lF6r1zO81/A8sO3ObCjFBhNYjNCC46', 0, NULL, NULL, NULL, 1, 'ab7522efef92579389500f4a88a830e1', NULL, NULL, 'jorge-gaitan-4', '', NULL, NULL, NULL, NULL, NULL, '2020-04-11 16:59:32'),
(6, 'usuarios', 'carlosroa', 'roacarlos1965@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aucfKA7IzVT/3iOQo4lfhWsxD1bbYDD.6', 1, 'I-HPL4YWW327VL', 1, '2020-04-13', 1, '9c1a7e3f6df6c982537bf93d1a3ccfda', 'vistas/img/usuarios/6/880.jpg', 'asda', 'asda', 'sb-hkl2i1378371@personal.example.com', 'Venezuela', 'Bolivarian Republic of', 'VE (324) 523-5324', '<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"207\" height=\"38\"><path stroke-linejoin=\"round\" stroke-linecap=\"round\" stroke-width=\"1\" stroke=\"#33\" fill=\"none\" d=\"M 1 1 c 0.04 0.14 0.61 6.26 2 8 c 2.06 2.58 6.47 5.35 10 7 c 6.13 2.86 13.15 5.17 20 7 c 8.24 2.2 16.6 4.17 25 5 c 11.87 1.17 23.77 1 36 1 c 5.1 0 10.95 0.16 15 -1 c 2.1 -0.6 6 -5 6 -5\"/><path stroke-linejoin=\"round\" stroke-linecap=\"round\" stroke-width=\"1\" stroke=\"#33\" fill=\"none\" d=\"M 107 13 c -0.14 0 -5.85 -0.76 -8 0 c -2.9 1.02 -6.44 3.9 -9 6 c -0.85 0.7 -2.43 2.57 -2 3 c 1.65 1.65 7.82 5.23 12 7 c 6.66 2.83 14.05 5.41 21 7 c 4.41 1.01 9.37 1 14 1 c 2.99 0 6.83 -0.1 9 -1 c 1.2 -0.5 2.77 -2.71 3 -4 c 0.33 -1.83 -0.66 -6.54 -1 -7 c -0.18 -0.23 -2.12 2.38 -2 3 c 0.12 0.62 1.92 1.7 3 2 c 7.97 2.2 17.41 4.83 26 6 c 5.79 0.79 12.39 0.75 18 0 c 3.94 -0.53 8.85 -2.32 12 -4 c 1.25 -0.66 3 -3.16 3 -4 c 0 -0.61 -1.97 -1.97 -3 -2 c -18.2 -0.54 -43.35 -0.3 -64 0 c -1.33 0.02 -4.19 0.93 -4 1 c 0.71 0.26 9.99 2.74 15 3 c 14.09 0.73 43 0 43 0\"/></svg>', '2020-04-13', '2020-04-13 22:35:00'),
(7, 'usuarios', 'gerardo', 'gerardo@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auMEo6F5gKzpwJtfl90a2/ie98ptT/GCq', 0, 'I-4WEC2LS0G3N5', NULL, '2020-04-14', 1, 'afe70e88714d261e19b246accad821b6', NULL, 'asda', 'asda', 'sb-hkl2i1378371@personal.example.com', 'Albania', 'AL', ' 355 (423) 432-4324', NULL, NULL, '2020-04-14 14:46:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id_video` int(11) NOT NULL,
  `id_cat` int(11) NOT NULL,
  `titulo_video` text NOT NULL,
  `descripcion_video` text NOT NULL,
  `medios_video` text NOT NULL,
  `imagen_video` text NOT NULL,
  `vista_gratuita` int(11) NOT NULL,
  `fecha_video` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id_video`, `id_cat`, `titulo_video`, `descripcion_video`, `medios_video`, `imagen_video`, `vista_gratuita`, `fecha_video`) VALUES
(1, 1, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae luctus mauris. Phasellus diam elit, congue interdum velit vitae, aliquet dignissim massa. Nam risus tortor, sagittis eget erat ac, sodales bibendum quam.', 'vistas/videos/cuerpo-activo/00-video.mp4', 'vistas/img/cuerpo-activo/01-imagen.jpg', 1, '2020-04-18 14:48:06'),
(2, 1, 'Consectetur adipiscing elit', 'Sed lacus purus, suscipit et nibh in, elementum varius purus. Mauris eget ornare ipsum, at faucibus est. Quisque sem elit, condimentum nec sodales et, auctor in nisl.', 'vistas/videos/cuerpo-activo/02-video.mp4', 'vistas/img/cuerpo-activo/01-video.mp4', 1, '2020-04-18 14:48:24'),
(3, 1, 'Morbi eleifend porta efficitur', 'Aenean quis lectus ac nibh elementum lobortis. Praesent ac bibendum nisi, in tempor elit. Cras ex diam, tincidunt congue tincidunt in, consequat quis lorem. Maecenas rutrum scelerisque dignissim.', 'vistas/videos/cuerpo-activo/03-video.mp4', 'vistas/img/cuerpo-activo/03-imagen.jpg', 1, '2019-07-10 18:52:40'),
(4, 1, 'Quisque nec tortor ultrices', 'Nunc suscipit suscipit porta. Donec sagittis urna hendrerit mauris suscipit, eu gravida lacus elementum. Donec augue lacus, ultricies quis hendrerit sit amet, varius sit amet justo.', 'vistas/videos/cuerpo-activo/04-video.mp4', 'vistas/img/cuerpo-activo/04-imagen.jpg', 0, '2019-07-10 18:52:40'),
(5, 1, 'Vivamus et ullamcorper massa', 'Praesent quis diam in diam facilisis semper. Nam sit amet commodo arcu. Duis luctus, purus vel imperdiet gravida, urna nibh tincidunt nisl', 'vistas/videos/cuerpo-activo/05-video.mp4', 'vistas/img/cuerpo-activo/05-imagen.jpg', 0, '2019-07-10 18:52:40'),
(6, 1, 'Erat sit amet urna consecteturt', 'Aliquam sed orci pretium, viverra orci sed, sollicitudin risus. Quisque velit ipsum, commodo ut venenatis non, bibendum vitae dolor. Nulla non porttitor nisi. In pharetra sed nisl eget fringilla. Suspendisse potenti.', 'vistas/videos/cuerpo-activo/06-video.mp4', 'vistas/img/cuerpo-activo/06-imagen.jpg', 0, '2019-07-10 18:52:40'),
(7, 2, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae luctus mauris. Phasellus diam elit, congue interdum velit vitae, aliquet dignissim massa. Nam risus tortor, sagittis eget erat ac, sodales bibendum quam.', 'vistas/videos/mente-sana/bowling.mp4', 'vistas/img/mente-sana/01-imagen.jpg', 1, '2020-04-18 14:49:24'),
(8, 2, 'Consectetur adipiscing elit', 'Sed lacus purus, suscipit et nibh in, elementum varius purus. Mauris eget ornare ipsum, at faucibus est. Quisque sem elit, condimentum nec sodales et, auctor in nisl.', 'vistas/videos/mente-sana/02-video.mp4', 'vistas/img/mente-sana/02-imagen.jpg', 1, '2019-07-10 18:52:40'),
(9, 2, 'Morbi eleifend porta efficitur', 'Aenean quis lectus ac nibh elementum lobortis. Praesent ac bibendum nisi, in tempor elit. Cras ex diam, tincidunt congue tincidunt in, consequat quis lorem. Maecenas rutrum scelerisque dignissim.', 'vistas/videos/mente-sana/03-video.mp4', 'vistas/img/mente-sana/03-imagen.jpg', 1, '2019-07-10 18:52:40'),
(10, 2, 'Quisque nec tortor ultrices', 'Nunc suscipit suscipit porta. Donec sagittis urna hendrerit mauris suscipit, eu gravida lacus elementum. Donec augue lacus, ultricies quis hendrerit sit amet, varius sit amet justo.', 'vistas/videos/mente-sana/04-video.mp4', 'vistas/img/mente-sana/04-imagen.jpg', 0, '2019-07-10 18:52:40'),
(11, 2, 'Vivamus et ullamcorper massa', 'Praesent quis diam in diam facilisis semper. Nam sit amet commodo arcu. Duis luctus, purus vel imperdiet gravida, urna nibh tincidunt nisl', 'vistas/videos/mente-sana/05-video.mp4', 'vistas/img/mente-sana/05-imagen.jpg', 0, '2019-07-10 18:52:40'),
(12, 2, 'Erat sit amet urna consecteturt', 'Aliquam sed orci pretium, viverra orci sed, sollicitudin risus. Quisque velit ipsum, commodo ut venenatis non, bibendum vitae dolor. Nulla non porttitor nisi. In pharetra sed nisl eget fringilla. Suspendisse potenti.', 'vistas/videos/mente-sana/06-video.mp4', 'vistas/img/mente-sana/06-imagen.jpg', 0, '2019-07-10 18:57:47'),
(13, 3, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae luctus mauris. Phasellus diam elit, congue interdum velit vitae, aliquet dignissim massa. Nam risus tortor, sagittis eget erat ac, sodales bibendum quam.', 'vistas/videos/espiritu-libre/01-video.mp4', 'vistas/img/espiritu-libre/01-imagen.jpg', 1, '2019-07-10 18:52:40'),
(14, 3, 'Consectetur adipiscing elit', 'Sed lacus purus, suscipit et nibh in, elementum varius purus. Mauris eget ornare ipsum, at faucibus est. Quisque sem elit, condimentum nec sodales et, auctor in nisl.', 'vistas/videos/espiritu-libre/02-video.mp4', 'vistas/img/espiritu-libre/02-imagen.jpg', 1, '2019-07-10 18:52:40'),
(15, 3, 'Morbi eleifend porta efficitur', 'Aenean quis lectus ac nibh elementum lobortis. Praesent ac bibendum nisi, in tempor elit. Cras ex diam, tincidunt congue tincidunt in, consequat quis lorem. Maecenas rutrum scelerisque dignissim.', 'vistas/videos/espiritu-libre/03-video.mp4', 'vistas/img/espiritu-libre/03-imagen.jpg', 1, '2019-07-10 18:52:40'),
(16, 3, 'Quisque nec tortor ultrices', 'Nunc suscipit suscipit porta. Donec sagittis urna hendrerit mauris suscipit, eu gravida lacus elementum. Donec augue lacus, ultricies quis hendrerit sit amet, varius sit amet justo.', 'vistas/videos/espiritu-libre/04-video.mp4', 'vistas/img/espiritu-libre/04-imagen.jpg', 0, '2019-07-10 18:52:40'),
(17, 3, 'Vivamus et ullamcorper massa', 'Praesent quis diam in diam facilisis semper. Nam sit amet commodo arcu. Duis luctus, purus vel imperdiet gravida, urna nibh tincidunt nisl', 'vistas/videos/espiritu-libre/05-video.mp4', 'vistas/img/espiritu-libre/05-imagen.jpg', 0, '2019-07-10 18:52:40'),
(18, 3, 'Erat sit amet urna consecteturt', 'Aliquam sed orci pretium, viverra orci sed, sollicitudin risus. Quisque velit ipsum, commodo ut venenatis non, bibendum vitae dolor. Nulla non porttitor nisi. In pharetra sed nisl eget fringilla. Suspendisse potenti.', 'vistas/videos/espiritu-libre/06-video.mp4', 'vistas/img/espiritu-libre/06-imagen.jpg', 0, '2019-07-10 18:57:47');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id_video`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
